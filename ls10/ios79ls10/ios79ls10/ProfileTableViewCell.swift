//
//  ProfleTableViewCell.swift
//  ios79ls10
//
//  Created by WA on 02.06.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

class ProfileTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var surnameLabel: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        print("Awake from nib")
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        nameLabel.text = "No name"
        surnameLabel.text = "No surname"
        profileImageView.image = UIImage(named: "profileOne")
    }

    func setupWith(profile: Profile) {
        if let name = profile.name {
            nameLabel.text = name
        }
        surnameLabel.text = profile.surname
        profileImageView.image = UIImage(named: profile.imageName)
    }
}
