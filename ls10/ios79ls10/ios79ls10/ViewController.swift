//
//  ViewController.swift
//  ios79ls10
//
//  Created by WA on 02.06.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!

    private var profiles: [Profile] = [
        Profile(name: "Ihor", surname: "Alekseev", imageName: "profileOne"),
        Profile(name: "Anna", surname: "Ignatieva", imageName: "profileTwo"),
        Profile(name: nil, surname: "Nilovna", imageName: "profileThree"),
        Profile(name: "Alina", surname: "Asmova", imageName: "profileThree"),
        Profile(name: "Alina", surname: "Asmova", imageName: "profileThree"),
        Profile(name: "Alina", surname: "Asmova", imageName: "profileThree"),
        Profile(name: nil, surname: "Nilovna", imageName: "profileThree"),
        Profile(name: "Alina", surname: "Asmova", imageName: "profileThree"),
        Profile(name: nil, surname: "Nilovna", imageName: "profileThree"),
        Profile(name: "Alina", surname: "Asmova", imageName: "profileThree"),
        Profile(name: "Alina", surname: "Asmova", imageName: "profileThree"),
        Profile(name: "Alina", surname: "Asmova", imageName: "profileThree"),
        Profile(name: "Alina", surname: "Asmova", imageName: "profileThree"),
        Profile(name: "Alina", surname: "Asmova", imageName: "profileThree"),
        Profile(name: "Alina", surname: "Asmova", imageName: "profileThree"),
        Profile(name: "Alina", surname: "Asmova", imageName: "profileThree")
    ]

    override func viewDidLoad() {
        super.viewDidLoad()

        // TableView Setup
        tableView.dataSource = self
        tableView.delegate = self
        tableView.tableFooterView = UIView()

        // Add custom cell to tableView
        let nib = UINib(nibName: "ProfileTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "CustomCell")
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if segue.identifier == "showSecondStep", let destinationVC = segue.destination as? SecondViewController {
            destinationVC.profiles = profiles//.filter { $0.name.contains("A") }
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let drawView = DrawView(frame: CGRect(x: 100, y: 100, width: 100, height: 100))
        view.addSubview(drawView)
    }
}

extension ViewController: UITableViewDelegate {
    
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return profiles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CustomCell", for: indexPath) as? ProfileTableViewCell else { fatalError() }
        cell.setupWith(profile: profiles[indexPath.row])
        return cell
    }
}
