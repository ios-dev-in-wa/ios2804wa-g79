//
//  AlertView.swift
//  ios79ls10
//
//  Created by WA on 02.06.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

class AlertView: UIView {

    @IBOutlet weak var alertStatusImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var doneButton: UIButton!

    private var doneAction: (() -> Void)?

    func setup(title: String, message: String, isNegative: Bool, doneAction: @escaping (() -> Void)) {
        titleLabel.text = title
        messageLabel.text = message
        alertStatusImageView.image = UIImage(named: isNegative ? "deleteImage" : "addImage")
        doneButton.backgroundColor = isNegative ? .red : .green
        self.doneAction = doneAction
    }

    @IBAction func doneButtonAction(_ sender: UIButton) {
        doneAction?()
    }
}
