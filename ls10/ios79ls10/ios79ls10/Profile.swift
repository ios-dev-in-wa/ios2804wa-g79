//
//  Profile.swift
//  ios79ls10
//
//  Created by WA on 02.06.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import Foundation

struct Profile {
    let name: String?
    let surname: String
    let imageName: String
}
