//
//  SecondViewController.swift
//  ios79ls10
//
//  Created by WA on 02.06.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!

    var profiles: [Profile] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // TableView Setup
        tableView.dataSource = self
        tableView.delegate = self
        tableView.tableFooterView = UIView()

        // Add custom cell to tableView
        let nib = UINib(nibName: "ProfileTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "CustomCell")
    }

    @IBAction func loginAction(_ sender: UIBarButtonItem) {

        // Custom alert controller
//        let viewController = UIViewController()
//        viewController.view.backgroundColor = UIColor.darkGray.withAlphaComponent(0.7)
//        let alertView: AlertView = AlertView.fromNib()
//
//        viewController.view.addSubview(alertView)
//
//        alertView.center = viewController.view.center
//
//        alertView.setup(title: "Registration is failed!",
//                        message: "Try again!",
//                        isNegative: true,
//                        doneAction: {
//                            viewController.dismiss(animated: true, completion: nil)
//        })
//        viewController.modalTransitionStyle = .crossDissolve
//        viewController.modalPresentationStyle = .overCurrentContext
//
//        present(viewController, animated: true, completion: nil)

        // Native alert controller
        let alertViewController = UIAlertController(title: "Registration is failed!", message: "Try again", preferredStyle: .alert)

        let cancelAction = UIAlertAction(title: "Press me", style: .destructive, handler: { _ in
            print("Cancelel button alert pressed")
        })

        let printAction = UIAlertAction(title: "Print", style: .default) { _ in
            print("Print button pressed")
        }

        alertViewController.addAction(cancelAction)
        alertViewController.addAction(printAction)
        alertViewController.preferredAction = printAction

        present(alertViewController, animated: true, completion: nil)
    }
}

extension SecondViewController: UITableViewDelegate {
    
}

extension SecondViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return profiles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CustomCell", for: indexPath) as? ProfileTableViewCell else { fatalError() }
        cell.setupWith(profile: profiles[indexPath.row])
        return cell
    }
}

