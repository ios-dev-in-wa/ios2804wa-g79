//
//  DrawView.swift
//  ios79ls10
//
//  Created by WA on 02.06.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

class DrawView: UIView {

    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        let path = UIBezierPath()
        path.move(to: CGPoint(x: 20, y: 20))
        path.addLine(to: CGPoint(x: 20, y: 60))
        path.addLine(to: CGPoint(x: 50, y: 66))
        path.addLine(to: CGPoint(x: 20, y: 20))
        path.close()

        UIColor.red.setFill()
        UIColor.green.setStroke()

        path.stroke()
        path.fill()

    }

}
