//
//  ViewController.swift
//  HWls2
//
//  Created by Ramin Akhmad on 01.05.2020.
//  Copyright © 2020 Ramin Akhmad. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var homeLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var centeralView: UIView!
    
    /// Constant offset for View
    let someInt = 100

    override func viewDidLoad() {
        super.viewDidLoad()

//        print(getCalculatedValue(x: 10, y: 12))
//        print(Int.max, Int.min)
//        print(Int.random(in: 0...10))
//        let inf = Double.pi / 0
//        print(inf.isInfinite ? "Ups, value is Infinitive" : inf)
//        print(switchIfExample(value: 20))
//        print(switchIfExample(value: 200))
//        print(switchBoolExample(x: 10, y: 20))
//        print(switchBoolExample(x: 30, y: 10))
//        if let greeting = getGreeting(name: "Nina") {
//            messageLabel.text = greeting
//            print(greeting)
//        }
//        if let greeting = getGreeting(name: "Artem") {
//            print(greeting)
//        }
//        let intFirstNumber = 20
//        print(String(intFirstNumber))
//        if let result = getDivision(firstNumber: Double(intFirstNumber), secondNumer: 10) {
//            print(result)
//        }
//        if let result = getDivision(firstNumber: Double(intFirstNumber), secondNumer: 10) {
//            print(result.exponent)
//        }
//        print(getDivision(firstNumber: Double(intFirstNumber), secondNumer: 0))
        
//        print(getGreeting(name: "Nina"))
//        print(getGreeting(name: "Artem"))
//        for i in 0...10 {
//            print(i)
//        }
//        centeralView.backgroundColor = UIColor.green
//        quizFour()
        quizTwo(number: 10)
    }
    
    /// This func returns String type value
    func getCalculatedValue(x: Int, y: Int) -> String {
        let value = x * y
        return value > 100 ? "Wow, result is - \(value)" : "Ha, result is - \(value)"
//        if value > 100 {
//            return "Wow, result is - \(value)"
//        } else {
//            return "Ha, result is - \(value)"
//        }
    }

    func switchIfExample(value: Int) -> String {
        switch value {
        case 0: return "It's zero"
        case 10: return "Wow, it's ten"
        case 20: return "Wow, it's 20"
        default: return "It's \(value)"
        }
//        if value == 0 {
//            return "It's zero"
//        } else if value == 10 {
//            return "Wow, it's ten"
//        } else if value == 20 {
//            return "Wow, it's 20"
//        }
//        return "It's \(value)"
    }

    func switchBoolExample(x: Int, y: Int) -> String {
        let result = x > y
        switch result {
        case true: return "X is bigger then Y"
        case false: return "Y is bigger then X"
        }
    }

    func getGreeting(name: String) -> String? {
        let greetingTemplate = "Hello, "
        switch name {
        case "Nina": return greetingTemplate + "Nina"
        case "Ihor": return greetingTemplate + "Ihor"
        default: return nil
        }
    }

    func getDivision(firstNumber: Double, secondNumer: Double) -> Double? {
        if secondNumer == 0 {
            return nil
        }
        return firstNumber / secondNumer
    }
    
//    Задача 2. Ежемесячная стипендия студента составляет 1300 гривен, а расходы на проживание превышают ее и составляют 2000 грн. в месяц. Рост цен ежемесячно увеличивает расходы на 6%. Определить, какую нужно иметь сумму денег, чтобы прожить учебный год (12 месяцев), используя только эти деньги и стипендию.

    func quizFour() {
        let stipuha: Double = 1300
        let costs: Double = 2000
        let inflyaziya = 1.06
        let monthes = 12

        var currentMoney: Double = 9840
        for i in 1...monthes {
            currentMoney += stipuha
            currentMoney -= costs * inflyaziya
            print(i, currentMoney)
        }
    }

    func quizTwo(number: Int) {
        for i in 0...number {
            print(i, number - i)
        }
    }
}
