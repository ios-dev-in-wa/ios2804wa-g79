//
//  SwiftRobotControlCenter.swift
//  MyRobot
//
//  Created by Ivan Vasilevich on 10/4/14.
//  Copyright (c) 2014 Ivan Besarab. All rights reserved.
//

import UIKit
//  All robot commands can be founded in GameViewController.h
class SwiftRobotControlCenter: RobotControlCenter {
    
    //  Level name setup
    override func viewDidLoad() {
        levelName = "L1C" //  Level name
        super.viewDidLoad()
    }
    
    // Write your code here
    override func run() {
//        if candyInBag {
//            move()
//        }
//        for _ in 0...7 {
//            findPeak()
//            goUp()
//            uTurn()
//            goDown()
//        }
    }

//    func

    func findPeak() {
        while frontIsClear {
            move()
        }
        turnRight()
    }

    func goUp() {
        while leftIsBlocked {
            move()
        }
    }

    func uTurn() {
        turnLeft()
        move()
        turnLeft()
    }

    func goDown() {
        while frontIsClear {
            move()
        }
        turnRight()
    }

    // Bla bla bla
    func forExample() {
        for _ in 0...4 {
            move()
            if candyPresent {
                break
            }
        }
    }

    /*
     asfas
     sdafsdf
     asdfd sfkewqmk
qwerqewr
     */
    func whileExample() {
        while frontIsClear {
            move()
            if candyPresent {
                break
            }
        }
    }

    func solveFirstPuzzle() {
        doubleMove()
        move()
        pick()
        move()
        move()
        put()
        turnRight()
        turnRight()
        turnRight()
    }

    func doubleMove() {
        move()
        move()
    }
    
    func turnLeft() {
        turnRight()
        turnRight()
        turnRight()
    }
}
