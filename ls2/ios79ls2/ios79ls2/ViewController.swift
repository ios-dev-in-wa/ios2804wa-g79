//
//  ViewController.swift
//  ios79ls2
//
//  Created by WA on 30.04.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
//        example()
//        let result = squareOfTheNumber(number: 16, isPrintNeeded: true)
//        print(result)
//        squareOfTheNumber(number: 3, isPrintNeeded: false)
//        squareOfTheNumber(number: 10000, isPrintNeeded: false)
//        let resultText = greeting(name: "Victor", count: 10)
//        print("------------")
//        print(resultText)
//        view.backgroundColor = UIColor.red
//        addBox()
        addBoxes(count: 4)
    }

    func example() {
        print("example func called")
        print("123123")
        print("56789kdd")
    }

    func squareOfTheNumber(number: Int, isPrintNeeded: Bool) -> String {
//        let firstNumber: Double = 1.5
//        print(firstNumber, Int(3.14))
//        let secondNumber = 3
//        secondNumber = Int(3.14)
//        let text = "example func called"
        if isPrintNeeded {
            print(number * number) // * / - + %
        }
        return "Square of the \(number), is - \(number * number)"
    }

    func greeting(name: String, count: Int) -> String {
        let text = "Hello \(name)"
        for _ in 0...count {
            print(text)
        }
        return text
    }

    func addBox() {
        let box = UIView()
        box.backgroundColor = UIColor.blue
        box.frame = CGRect(x: 50, y: 50, width: 50, height: 50)
        view.addSubview(box)
        let insideBox = UIView()
        insideBox.backgroundColor = UIColor.black
        insideBox.frame = CGRect(x: 10, y: 10, width: 10, height: 10)
        box.addSubview(insideBox)
    }

    func addBoxes(count: Int) {
        let height = 50
        let width = 50
        let offset = 10
        var counter = 0
        for _ in 0..<count {
            let box = UIView()
            box.backgroundColor = UIColor.blue
            let xCord = width + width * counter + counter * offset
            box.frame = CGRect(x: xCord, y: 50, width: width, height: height)
            box.layer.cornerRadius = 10//CGFloat(height / 2)
            view.addSubview(box)
            counter += 1
        }
    }

}

