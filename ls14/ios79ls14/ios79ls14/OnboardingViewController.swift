//
//  OnboardingViewController.swift
//  ios79ls14
//
//  Created by WA on 16.06.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit
import Kingfisher

class OnboardingViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var shadowView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imageView.kf.indicatorType = .activity
        imageView.layer.shadowOffset = .init(width: 10, height: 10)
        imageView.layer.shadowOpacity = 1
        imageView.layer.shadowColor = UIColor.red.cgColor
        imageView.layer.shadowRadius = 10
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        shadowView.layer.shadowColor = UIColor.black.cgColor
        shadowView.layer.shadowOpacity = 1
        shadowView.layer.shadowOffset = .init(width: 10, height: 10)
        shadowView.layer.shadowRadius = 10
    }

    func setupWith(model: OnboardingModel) {
        imageView.kf.setImage(with: URL(string: model.imageName))//image = UIImage(named: model.imageName)
        titleLabel.text = model.titleText
    }
}
