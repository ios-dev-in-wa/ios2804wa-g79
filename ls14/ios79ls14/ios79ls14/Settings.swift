//
//  Settings.swift
//  ios79ls14
//
//  Created by WA on 16.06.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import Foundation

final class Settings {

    static let shared = Settings()

    private init() {
        let appDefaults = [String:AnyObject]()
        UserDefaults.standard.register(defaults: appDefaults)

        NotificationCenter.default.addObserver(self, selector: #selector(settingChanged(notification:)), name: UserDefaults.didChangeNotification, object: nil)
    }



    var soundSelectedValue: String {
        get {
            return UserDefaults.standard.string(forKey: "selectedSoundID") ?? "0"
        }
        set {
            UserDefaults.standard.setValue(newValue, forKey: "selectedSoundID")
        }
    }
    
    var soundEnabled: Bool {
        get {
            UserDefaults.standard.bool(forKey: "enabled_preference")
        }
        set {
            UserDefaults.standard.setValue(newValue, forKey: "enabled_preference")
        }
    }

    var name: String? {
        get {
            UserDefaults.standard.value(forKey: "name_preference") as? String
        }
    }

    @objc private func settingChanged(notification: Notification) {
        if let defaults = notification.object as? UserDefaults {
            print(defaults.value(forKey: "selectedSoundID"), defaults.value(forKey: "enabled_preference"), defaults.value(forKey: "name_preference"))
        }
    }
}
