//
//  ViewController.swift
//  ios79ls14
//
//  Created by WA on 16.06.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var pageControll: UIPageControl!
    var pageViewContoller: UIPageViewController?

    var onboardingModels = [
        OnboardingModel(imageName: "https://images.hdqwalls.com/wallpapers/retrowave-car-4k-fr.jpg", titleText: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."),
        OnboardingModel(imageName: "https://akm-img-a-in.tosshub.com/sites/btmt/images/stories/lamborghini_660_140220101539.jpg", titleText: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."),
        OnboardingModel(imageName: "https://cars.usnews.com/dims4/USNEWS/c63efbe/2147483647/resize/640x420%3E/format/jpeg/quality/85/?url=https%3A%2F%2Fcars.usnews.com%2Fstatic%2Fimages%2Farticle%2F202004%2F126924%2F02_2019_Acura_RDX_Advance_1crop-min_640x420.jpg", titleText: "This car is too coooooool! PORSCHE")
    ]

    var pages: [UIViewController] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        pages = onboardingModels.compactMap { self.craeteContollerWith(model: $0) }
        pageViewContoller?.setViewControllers([pages[0]], direction: .forward, animated: true, completion: nil)
        pageControll.numberOfPages = pages.count

        let appDefaults = [String:AnyObject]()
        UserDefaults.standard.register(defaults: appDefaults)
        NotificationCenter.default.addObserver(self, selector: #selector(settingChanged(notification:)), name: UserDefaults.didChangeNotification, object: nil)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if segue.identifier == "showPageViewController", let pageVC = segue.destination as? UIPageViewController {
            pageViewContoller = pageVC
            pageViewContoller?.delegate = self
            pageViewContoller?.dataSource = self
        }
    }

    @IBAction func pageControlAction(_ sender: UIPageControl) {
        scrollToViewController(index: sender.currentPage)
//        Settings.shared.soundSelectedValue = "1"
        print("Selected sound value \(Settings.shared.soundSelectedValue)")
        print("Selected sound value \(Settings.shared.soundEnabled)")
    }

    private func craeteContollerWith(model: OnboardingModel) -> UIViewController? {
        guard let viewController = storyboard?.instantiateViewController(withIdentifier: "OnboardingViewController") as? OnboardingViewController else { return nil }
        _ = viewController.view
        viewController.setupWith(model: model)
        return viewController
    }

    func scrollToViewController(index newIndex: Int) {
        if let firstViewController = pageViewContoller?.viewControllers?.first,
            let currentIndex = pages.firstIndex(of: firstViewController) {
            let direction: UIPageViewController.NavigationDirection = newIndex >= currentIndex ? .forward : .reverse
                let nextViewController = pages[newIndex]
                scrollToViewController(viewController: nextViewController, direction: direction)
        }
    }

    private func scrollToViewController(viewController: UIViewController,
                                        direction: UIPageViewController.NavigationDirection = .forward) {
        pageViewContoller?.setViewControllers([viewController], direction: direction, animated: true, completion: nil)
    }

    @IBAction func goToSettingsAction(_ sender: UIButton) {
        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
            return
        }

        if UIApplication.shared.canOpenURL(settingsUrl) {
            UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
//                print("Settings opened: \(success)") // Prints true
            })
        }
    }

    @objc private func settingChanged(notification: Notification) {
        if let defaults = notification.object as? UserDefaults {
            print(defaults.value(forKey: "selectedSoundID"), defaults.value(forKey: "enabled_preference"), defaults.value(forKey: "name_preference"))
        }
    }
}

extension ViewController: UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        if let viewController = pendingViewControllers.first, let index = pages.firstIndex(of: viewController) {
            pageControll.currentPage = index
        }
    }
}

extension ViewController: UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let currentIndex = pages.firstIndex(of: viewController) else { return nil }
        if currentIndex == 0 {
            return nil
        } else {
            return pages[currentIndex - 1]
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let currentIndex = pages.firstIndex(of: viewController) else { return nil }
        if currentIndex == pages.count - 1 {
            return nil
        } else {
            return pages[currentIndex + 1]
        }
    }
}
