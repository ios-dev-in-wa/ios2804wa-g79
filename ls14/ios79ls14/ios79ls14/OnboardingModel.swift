//
//  OnboardingModel.swift
//  ios79ls14
//
//  Created by WA on 16.06.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import Foundation

struct OnboardingModel {
    let imageName: String
    let titleText: String
}
