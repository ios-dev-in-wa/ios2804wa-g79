//
//  CRUDProtocol.swift
//  g79ls8
//
//  Created by WA on 26.05.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import Foundation

class Road { }

protocol RoadProtocol {
    func showRoad(road: Road)
    func navigateThorugh(road: Road)
}

protocol CRUDProtocol {
    func createName(name: String)
    func readFromDB() -> String?
    func updateValue(name: String)
    func removeName()
}

class UserDefaultsStorage: CRUDProtocol {

    private let defaults = UserDefaults.standard
    private let nameKey = "defaultsName"

    func createName(name: String) {
        print("Create \(name) on defaults")
        defaults.set(name, forKey: nameKey)
    }
    
    func readFromDB() -> String? {
        print("read value from defaults")
        return defaults.string(forKey: nameKey)
    }
    
    func updateValue(name: String) {
        print("Update \(name) on defaults")
        defaults.set(name, forKey: nameKey)
    }
    
    func removeName() {
        print("remove value from defaults")
        defaults.removeObject(forKey: nameKey)
    }
}

class ServerStorage: CRUDProtocol {

    func createName(name: String) {
        print("Create \(name) on server")
//        defaults.set(name, forKey: nameKey)
    }
    
    func readFromDB() -> String? {
        print("read value from server")
        return ""
//        defaults.string(forKey: nameKey)
    }
    
    func updateValue(name: String) {
        print("Update \(name) on server")
//        defaults.set(name, forKey: nameKey)
    }
    
    func removeName() {
        print("remove value from server")
//        defaults.removeObject(forKey: nameKey)
    }
}
