//
//  Seasons.swift
//  g79ls8
//
//  Created by WA on 26.05.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

protocol Seasons {
    func winterIsComing()
    func springIsComing()
    func summerIsComing()
    func autumnIsComing()
}

class Rabbit: Seasons {
    var shckuraColor: UIColor = .gray

    func winterIsComing() {
        print("Winter is here, rabbit is changing shkura")
        shckuraColor = .white
    }
    
    func springIsComing() {
        print("Sping is here")
        shckuraColor = .gray
    }
    
    func summerIsComing() {
        print("Summper is here\n", "Rabbit do nothing")
    }
    
    func autumnIsComing() {
        print("Autumn is here\n", "Rabbit do nothing")
    }
}

class Bear: Seasons {

    func goToBerlogaAndSleep() {
        print("Sleeping Zzzz")
    }

    func findFood() {
        print("Hunting....")
    }
    
    func winterIsComing() {
        print("Winter is here, bear is going to sleep")
        goToBerlogaAndSleep()
    }
    
    func springIsComing() {
        print("Sping is here")
        findFood()
    }
    
    func summerIsComing() {
        print("Summper is here")
        findFood()
    }
    
    func autumnIsComing() {
        print("Autumn is here")
        findFood()
    }
}
