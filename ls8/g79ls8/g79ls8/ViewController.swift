//
//  ViewController.swift
//  g79ls8
//
//  Created by WA on 26.05.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var localSaverSwitch: UISwitch!
    @IBOutlet weak var textField: UITextField!
    
    let boxView = UIView()
    let rabbit = Rabbit()
    let bear = Bear()
    var closure: ((String) -> String)?
    var arrayOfNames = ["Ihor", "Alla", "Nikita"]

    var storage: CRUDProtocol = UserDefaultsStorage()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
//        playSeasons()
        print(storage.readFromDB())
        print(Date())
        closure = { name in
            return "HELLO \(name) I am from closure"
        }

        printSomeGoodNews(completion: {
            print("All news are printed")
        })

        boxView.frame.size = CGSize(width: 100, height: 100)
        boxView.backgroundColor = .black
        view.addSubview(boxView)
        boxView.center = view.center

        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(panGestureAction))
        boxView.addGestureRecognizer(panGesture)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if segue.identifier == "goToNextVC", let nextVC = segue.destination as? NextViewController {
            // Force load view
//            _ = nextVC.view
            nextVC.didChangeColor = { color in
                self.view.backgroundColor = color
            }
            nextVC.didSaveText = { text in
                self.arrayOfNames.append(text)
                print(self.arrayOfNames)
            }
            nextVC.getItemFromArray = { index in
                guard index <= self.arrayOfNames.count - 1 else { return "No name" }
                return self.arrayOfNames[index]
            }
        }
    }

    var startBoxPosition = CGPoint()
    @objc func panGestureAction(sender: UIPanGestureRecognizer) {
        switch sender.state {
        case .possible:
            print("Possible")
        case .began:
            sender.location(in: self.view)
            view.backgroundColor = .lightGray
            startBoxPosition = boxView.center
            print("Began")
        case .changed:
            print(sender.translation(in: view), sender.velocity(in: view))
            UIView.animate(withDuration: 0.5) {
                self.boxView.center = sender.location(in: self.view)
            }
            print("Changed")
        case .ended:
            view.backgroundColor = .white
            UIView.animate(withDuration: 0.5) {
                self.boxView.center = self.startBoxPosition
            }
            print("Ended")
        case .cancelled:
            print("Cancelled")
        case .failed:
            print("Failed")
        @unknown default: print("Default")
        }
    }

    func playSeasons() {
        let animals: [Seasons] = [rabbit, bear]
        animals.forEach { $0.winterIsComing() }
    }

    func printSomeGoodNews(completion: () -> Void) {
        for _ in 0...10 {
            print("Good News")
        }
        completion()
    }
    
    @IBAction func switchValueChanged(_ sender: UISwitch) {
        storage = sender.isOn ? ServerStorage() : UserDefaultsStorage()
    }
    
    @IBAction func saveAction(_ sender: UIButton) {
        print(closure?("Artem"))
        storage.createName(name: textField.text ?? "Artem")
    }

    @IBAction func didPressSettings(_ sender: UIButton) {
        performSegue(withIdentifier: "goToNextVC", sender: nil)
    }

    @IBAction func didTapOnView(_ sender: UITapGestureRecognizer) {
        switch sender.state {
        case .possible:
            print("Possible")
        case .began:
            print("Began")
        case .changed:
            print("Changed")
        case .ended:
            print("Ended")
        case .cancelled:
            print("Cancelled")
        case .failed:
            print("Failed")
        @unknown default: print("Default")
        }
        navigationController?.navigationBar.isHidden.toggle()
        view.backgroundColor = .random
        print("did tap")
    }


    @IBAction func pinchAction(_ sender: UIPinchGestureRecognizer) {
//        if sender.state == .changed {
//
//        }
        switch sender.state {
        case .possible:
            print("Possible")
        case .began:
            print("Began")
        case .changed:
            print(sender.scale, sender.velocity)
            print(boxView.contentScaleFactor)
            boxView.transform = .init(scaleX: sender.scale, y: sender.scale)
            print("Changed")
        case .ended:
            print("Ended")
//            UIView.animate(withDuration: 0.5) {
//                self.boxView.transform = .identity
//            }
        case .cancelled:
            print("Cancelled")
        case .failed:
            print("Failed")
        @unknown default: print("Default")
        }
    }

    @IBAction func rotationAction(_ sender: UIRotationGestureRecognizer) {
        switch sender.state {
        case .possible:
            print("Possible")
        case .began:
            print("Began")
        case .changed:
            //                    print(sender.scale, sender.velocity)
            print(boxView.contentScaleFactor)
            boxView.transform = .init(rotationAngle: sender.rotation)
            print("Changed")
        case .ended:
            print("Ended")
            //            UIView.animate(withDuration: 0.5) {
            //                self.boxView.transform = .identity
        //            }
        case .cancelled:
            print("Cancelled")
        case .failed:
            print("Failed")
        @unknown default: print("Default")
        }
    }
    
    
    @IBAction func swipeAction(_ sender: UISwipeGestureRecognizer) {
        switch sender.state {
        case .possible:
            print("Possible")
        case .began:
            print("Began")
        case .changed:
            //                    print(sender.scale, sender.velocity)
            print(boxView.contentScaleFactor)
//            boxView.transform = .init(rotationAngle: sender.rotation)
            print("Changed")
        case .ended:
            print("Swipe Ended")
            //            UIView.animate(withDuration: 0.5) {
            //                self.boxView.transform = .identity
        //            }
        case .cancelled:
            print("Cancelled")
        case .failed:
            print("Failed")
        @unknown default: print("Default")
        }
        
    }
}

extension CGFloat {
    static var random: CGFloat {
        return CGFloat(arc4random()) / CGFloat(UInt32.max)
    }
}

extension UIColor {
    static var random: UIColor {
        return UIColor(red: .random, green: .random, blue: .random, alpha: 1.0)
    }
}
