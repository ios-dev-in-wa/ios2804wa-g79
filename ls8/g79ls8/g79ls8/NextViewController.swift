//
//  NextViewController.swift
//  g79ls8
//
//  Created by WA on 26.05.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

class NextViewController: UIViewController {

    @IBOutlet weak var nameLabel: UILabel!

    var didChangeColor: ((UIColor) -> Void)?
    var didSaveText: ((String) -> Void)?
    var getItemFromArray: ((Int) -> String)?

    override func viewDidLoad() {
        super.viewDidLoad()
        if let name = getItemFromArray?(1) {
            nameLabel.text = name
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        UIView.animate(withDuration: 2.0) {
//            self.view.backgroundColor = .red
//            self.nameLabel.center = self.view.center
//            self.nameLabel.frame = CGRect(x: 20, y: 100, width: 50, height: 50)
//            self.nameLabel.alpha = 0
//            self.nameLabel.transform = .init(scaleX: 0.1, y: 0.5)
//            self.nameLabel.transform = .init(translationX: 20, y: 50)
//            self.nameLabel.transform = .init(rotationAngle: .pi / 2)
//        }

//        UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: [.autoreverse, .repeat], animations: {
//            self.nameLabel.transform = .init(rotationAngle: .pi / 2)
//        }, completion: nil)

//        UIView.animate(withDuration: 1, delay: 0, options: [], animations: {
////            self.nameLabel.transform = .init(rotationAngle: .pi / 2)
//            self.nameLabel.transform = CGAffineTransform.init(rotationAngle: .pi / 2).scaledBy(x: 2, y: 2).translatedBy(x: 50, y: -50)
//        }, completion: { isFinished in
//            self.nameLabel.transform = .identity
//            if isFinished {
//                print("Animation is finished")
//            }
//        })

//        UIView.transition(with: view, duration: 1, options: [.transitionFlipFromLeft], animations: {
//            self.nameLabel.text = "Ha ha ha ha ha ha"
//            self.nameLabel.transform = CGAffineTransform.init(rotationAngle: .pi / 2).scaledBy(x: 2, y: 2).translatedBy(x: 50, y: -50)
//        }, completion: nil)
//        print("Animation is finished?")
    }

    deinit {
        print("DEINITED")
    }

    @IBAction func didPressColorButton(_ sender: UIButton) {
        didChangeColor?(sender.backgroundColor ?? .black)
    }

    @IBAction func saveNameAction(_ sender: UIButton) {
        didSaveText?(nameLabel.text ?? "No name is available")
    }
}
