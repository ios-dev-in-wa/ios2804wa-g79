//
//  SwapiPeopleResponse.swift
//  Swapi
//
//  Created by WA on 09.06.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import Foundation

// MARK: - SwapiPeopleResponse
struct SwapiPeopleResponse: Codable {
    let count: Int
    let next: String?
    let previous: String?
    let results: [Puple]
}

// MARK: - Result
struct Puple: Codable {
    let name, height, mass, hairColor: String?
    let skinColor, eyeColor, birthYear: String?
    let gender: String?
    let homeworld: String?
    let films, species, vehicles, starships: [String]?
    let created, edited: String?
    let url: String?

    enum CodingKeys: String, CodingKey {
        case name, height, mass
        case hairColor = "hair_color"
        case skinColor = "skin_color"
        case eyeColor = "eye_color"
        case birthYear = "birth_year"
        case gender, homeworld, films, species, vehicles, starships, created, edited, url
    }
}

//enum Gender: String, Codable {
//    case female = "female"
//    case male = "male"
//    case nA = "n/a"
//    case hermaphrodite = "hermaphrodite"
//}
