//
//  SwapiService.swift
//  Swapi
//
//  Created by WA on 09.06.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import Foundation

struct REST {
    static let baseURL = URL(string: "swapi.dev")!
}

struct SwapiPeopleService {

    private let session = URLSession.shared

    private let endpoint = "people"

    func getPeoples(page: Int, completion: @escaping (([Puple], Bool) -> Void)) {
//        "http://swapi.dev/api/people/?page=2
        var components = URLComponents()
        components.host = REST.baseURL.absoluteString
        components.scheme = "https"
        components.path = "/api/people"
        components.queryItems = [
           URLQueryItem(name: "page", value: String(page))
//           URLQueryItem(name: "format", value: format)
        ]
        print(components.url)
        var request = URLRequest(url: components.url!)

        request.httpMethod = "GET"

        session.dataTask(with: request) { data, response, error in
            if let error = error {
                print(error.localizedDescription)
                return
            }

            guard let response = response as? HTTPURLResponse, let data = data else { return }
            switch response.statusCode {
            case 200:
                try? data.write(to: URL(fileURLWithPath: "/Users/wa/Desktop/saved.txt"))
                print(String(data: data, encoding: .utf8))
//                let decoder = JSONDecoder()
                do {
                    let responseData = try JSONDecoder().decode(SwapiPeopleResponse.self, from: data)
                    print(responseData)
                    DispatchQueue.main.async {
                        completion(responseData.results, responseData.next == nil)
                    }
                } catch {
                    print(error.localizedDescription)
                }
            default:
                print("UNEXPECTED STATUS CODE: \(response.statusCode)")
            }
            
        }.resume()
    }
}
