//
//  ViewController.swift
//  Swapi
//
//  Created by WA on 09.06.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!

    private var peoples = [Puple]()

    private let service = SwapiPeopleService()
    private var currentPage = 1
    private var isListCompleted = false

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        loadMore()
    }

    func loadMore() {
        guard !isListCompleted else {
            print("List is finished")
            return
        }
        service.getPeoples(page: currentPage) { [weak self] results, isFinished in
            self?.isListCompleted = isFinished
            self?.peoples += results
            self?.tableView.reloadData()
            self?.currentPage += 1
        }
    }
}

extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == peoples.count - 5 {
            loadMore()
        }
    }
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return peoples.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "peopleCell", for: indexPath)
        cell.textLabel?.text = peoples[indexPath.row].name
        cell.detailTextLabel?.text = peoples[indexPath.row].homeworld
        return cell
    }
    
    
}
