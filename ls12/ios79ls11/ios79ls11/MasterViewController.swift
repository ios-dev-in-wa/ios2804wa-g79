//
//  MasterViewController.swift
//  ios79ls11
//
//  Created by WA on 04.06.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit
import Parse

class MasterViewController: UITableViewController {

    var detailViewController: DetailViewController? = nil
//    var objects = [Any]()
    var carObjects = [Car]()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        navigationItem.leftBarButtonItem = editButtonItem

//        setupNavigationButtons()
        if let split = splitViewController {
            let controllers = split.viewControllers
            detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? DetailViewController
        }
//        objects = [Date(), Date(), Date(), Date()]
//        updateCarList()
    }

    override func viewWillAppear(_ animated: Bool) {
        clearsSelectionOnViewWillAppear = splitViewController!.isCollapsed
        super.viewWillAppear(animated)
        updateCarList()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        detailViewController?.detailDescriptionLabel?.text = (objects[1] as? Date)?.description

//        let controller = SPPermissions.dialog([.contacts, .photoLibrary])
//
//        // Ovveride texts in controller
//        controller.titleText = "Title Text"
//        controller.headerText = "Header Text"
//        controller.footerText = "Footer Text"
//
//        // Set `DataSource` or `Delegate` if need.
//        // By default using project texts and icons.
////        controller.dataSource = self
////        controller.delegate = self
//
//        // Always use this method for present
//        controller.present(on: self)
        
        // PARSE LOGIN
//        Parse
    }

//    @objc
//    func insertNewObject(_ sender: Any) {
//        objects.insert(NSDate(), at: 0)
//        let indexPath = IndexPath(row: 0, section: 0)
//        tableView.insertRows(at: [indexPath], with: .automatic)
//    }

    func updateCarList() {
        CarManager.shared.getCars { cars in
            self.carObjects = cars
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }

    // MARK: - Segues

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            if let indexPath = tableView.indexPathForSelectedRow {
//                let object = objects[indexPath.row] as! NSDate
                let controller = (segue.destination as! UINavigationController).topViewController as! DetailViewController
//                controller.detailItem = object
                controller.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
                controller.navigationItem.leftItemsSupplementBackButton = true
                detailViewController = controller
            }
        } else if segue.identifier == "showAddVC" {
            guard let controller = segue.destination as? AddCarViewController else { return }
            if let lastCar = sender as? Car {
                controller.isInEditMode = true
                controller.currentCar = lastCar
            }
            controller.didSaveObject = {
                self.updateCarList()
            }
        }
    }

    // MARK: - Table View

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return carObjects.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        let object = carObjects[indexPath.row]
        cell.textLabel!.text = object.title
        cell.detailTextLabel?.text = object.wasInAccident ? "Crashed" : "Like a New"
        return cell
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            CarManager.shared.delete(car: carObjects[indexPath.row])
            carObjects.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
        }
    }

    override func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let editAction = UIContextualAction(style: .normal, title: "Edit") { _, _, _ in
            self.performSegue(withIdentifier: "showAddVC", sender: self.carObjects[indexPath.row])
        }
        return UISwipeActionsConfiguration(actions: [editAction])
    }
    
    func setupNavigationButtons() {
        let barButton = UIBarButtonItem(barButtonSystemItem: .bookmarks, target: self, action: #selector(didPressBookmarks))
//        let addButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(insertNewObject(_:)))
        navigationItem.rightBarButtonItems = [barButton]
    }

    @objc func didPressBookmarks() {
        performSegue(withIdentifier: "masterGoToNextVC", sender: nil)
    }

}


//extension MasterViewController: SPPermissionsDataSource {
//    func configure(_ cell: SPPermissionTableViewCell, for permission: SPPermission) -> SPPermissionTableViewCell {
//        
//    }
//    
//    
//}
//
//extension MasterViewController: SPPermissionsDelegate {
//    
//}
