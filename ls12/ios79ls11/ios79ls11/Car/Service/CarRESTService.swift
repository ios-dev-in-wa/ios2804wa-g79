//
//  CarRESTService.swift
//  ios79ls11
//
//  Created by WA on 09.06.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import Foundation
import Parse

struct REST {
    static let mainUrl = URL(string: "https://parseapi.back4app.com/classes/")!
    static let apiKey = "oifEWCTWyiCL11AqSjnV8Nroamyy6Nq3PeDSqgq7"
    static let applicationId = "iYGB2cjSRnCf8Oa2v7j1qPzODWHyPu7WC1Okk6pk"
}

protocol CarService {
    func createCar(car: Car, completion: ((Car?) -> Void)?)
    func readCars(completion: (([Car]) -> Void)?)
    func updateCar(car: Car, completion: ((Car) -> Void)?)
    func deleteCar(car: Car, completion: ((Car) -> Void)?)
}

struct CarRESTService: CarService {

    private let session = URLSession.shared

    func createCar(car: Car, completion: ((Car?) -> Void)?) {
        var request = requestWithMethod(method: "POST")
        let data = try! JSONEncoder().encode(car)
        try? data.write(to: URL(fileURLWithPath: "/Users/wa/Desktop/saved.txt"))
        request.httpBody = data//try? JSONEncoder().encode(car)

        session.dataTask(with: request) { (data, resposne, error) in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            guard let resposne = resposne as? HTTPURLResponse, let data = data else { return }
            switch resposne.statusCode {
            case 200, 201:
                print(String(data: data, encoding: .utf8))
                DispatchQueue.main.async {
                    completion?(nil)
                }
            default:
                print(resposne.statusCode)
            }
        }.resume()
    }
    
    func readCars(completion: (([Car]) -> Void)?) {
        let request = requestWithMethod(method: "GET")

        let task = session.dataTask(with: request) { data, response, error in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            guard let resposne = response as? HTTPURLResponse, let data = data else { return }
            switch resposne.statusCode {
            case 200:
                try? data.write(to: URL(fileURLWithPath: "/Users/wa/Desktop/saved.txt"))
                print(String(data: data, encoding: .utf8))
                let decoder = JSONDecoder()
                do {
                    let result = try decoder.decode(CarResponse.self, from: data)
                    DispatchQueue.main.async {
                        completion?(result.results)
                    }
                } catch {
                    print(error.localizedDescription)
                }
            default:
                print(resposne.statusCode)
            }
        }
        task.resume()
    }
    
    func updateCar(car: Car, completion: ((Car) -> Void)?) {
        
    }
    
    func deleteCar(car: Car, completion: ((Car) -> Void)?) {
        
    }

    private func requestWithMethod(method: String) -> URLRequest {
        var request = URLRequest(url: REST.mainUrl.appendingPathComponent("Car"))
        request.httpMethod = method
        request.addValue(REST.applicationId, forHTTPHeaderField: "X-Parse-Application-Id")
        request.addValue(REST.apiKey, forHTTPHeaderField: "X-Parse-REST-API-Key")
        if method == "POST" {
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        }
        return request
    }
}
