//
//  ProfileModel.swift
//  ios79ls9
//
//  Created by WA on 28.05.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import Foundation

struct ProfileModel {
    var name: String
    let surname: String
    let dateOfBirth: String
}

class ProfileClass: Codable {
    var name: String = "Placeholder"
    let surname: String = ""
    let dateOfBirth: String = ""

}
