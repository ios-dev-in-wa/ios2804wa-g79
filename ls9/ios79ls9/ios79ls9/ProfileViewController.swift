//
//  ProfileViewController.swift
//  ios79ls9
//
//  Created by WA on 28.05.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var surnameLabel: UILabel!
    @IBOutlet weak var dateOfBirthLabel: UILabel!
    
    func setupWith(profile: ProfileModel) {
        nameLabel.text = profile.name
        surnameLabel.text = profile.surname
        dateOfBirthLabel.text = profile.dateOfBirth
    }
}
