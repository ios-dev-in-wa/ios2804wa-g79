//
//  ViewController.swift
//  ios79ls9
//
//  Created by WA on 28.05.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!

    var arrayOfNames: [ProfileModel] = [
        ProfileModel(name: "Ihor", surname: "Abramovich", dateOfBirth: "25.05.02"),
        ProfileModel(name: "Alla", surname: "Abramovich", dateOfBirth: "25.03.02"),
        ProfileModel(name: "Artem", surname: "Ignatovych", dateOfBirth: "5.05.85")
    ]

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self

//        tableView.isEditing = true

        
        /// Version check!
//        if #available(iOS 13, *) {
//            
//        } else {
//            // show sad face emoji
//        }

        // VALUE TYPE
        var profileStructOne = ProfileModel(name: "Artem", surname: "", dateOfBirth: "")
        let profuleStructTwo = profileStructOne
        profileStructOne.name = "Ihor"
        print(profileStructOne.name, profuleStructTwo.name)

        // CLASS TYPE
        let profileClassOne = ProfileClass()
        print(profileClassOne.name)
        let profileClassTwo = profileClassOne
        profileClassOne.name = "Kapitoshka"
        print(profileClassOne.name, profileClassTwo.name)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if segue.identifier == "goToRedVC",
            let destinationVC = segue.destination as? ProfileViewController,
            let value = sender as? ProfileModel {
            _ = destinationVC.view
            destinationVC.setupWith(profile: value)
        }
    }
}

extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.row % 2 == 0 ? 50 : 100
    }

//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return 200
//    }

    func tableView(_ tableView: UITableView, accessoryButtonTappedForRowWith indexPath: IndexPath) {
        print("SHOW DETAILS, \(indexPath.row)")
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "goToRedVC", sender: arrayOfNames[indexPath.row])
//        switch indexPath.row {
//        case 0:
//            performSegue(withIdentifier: "goToStaticVC", sender: nil)
//        case 1:
//            performSegue(withIdentifier: "goToRedVC", sender: nil)
//        default: print("Bad indexPath")
//        }
//        print("user did press cell at \(indexPath.row)")
    }

    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .insert
    }

    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return "ARE YOU SURE???"
    }

    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let action = UIContextualAction(style: .destructive, title: "Trash") { (_, _, _) in
            self.arrayOfNames.remove(at: indexPath.row)
            self.tableView.reloadData()
        }
        let copyAction = UIContextualAction(style: .normal, title: "Make copy") { (_, _, _) in
            self.arrayOfNames.append(self.arrayOfNames[indexPath.row])
            self.tableView.reloadData()
        }
        return UISwipeActionsConfiguration(actions: [action, copyAction])
    }
    
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayOfNames.count//section == 0 ? 5 : 10
    }

//    func numberOfSections(in tableView: UITableView) -> Int {
//        return 2
//    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return section == 0 ? "First section" : "Second section"
    }

    func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        return section == 0 ? "First footer" : "Second footer"
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "basicCell", for: indexPath)
//        print(indexPath.row, indexPath.section, cell)
        let value = arrayOfNames[indexPath.row]
        cell.textLabel?.text = value.name//"section index:\(indexPath.section)"
        cell.detailTextLabel?.text = value.surname//"section index:\(indexPath.section) row:\(indexPath.row)"
        return cell
    }

//    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
//        return indexPath.row == 2 && indexPath.section == 0
//    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        switch editingStyle {
        case .delete:
            arrayOfNames.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .automatic)
            print("Delete")
        case .insert:
            print("Insert")
            tableView.insertRows(at: [indexPath], with: .automatic)
        case .none:
            print("None")
        @unknown default:
            fatalError()
        }
//        tableView.reloadData()
//        tableView.reloadSections([0], with: .automatic)
    }

    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let value = arrayOfNames[sourceIndexPath.row]
        arrayOfNames.remove(at: sourceIndexPath.row)
        arrayOfNames.insert(value, at: destinationIndexPath.row)
        print(arrayOfNames)
        tableView.reloadData()
    }
}
