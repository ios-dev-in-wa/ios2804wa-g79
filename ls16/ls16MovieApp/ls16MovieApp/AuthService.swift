//
//  AuthService.swift
//  ls16MovieApp
//
//  Created by WA on 23.06.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import Foundation

struct Utils {
    static let mainURL = "www.omdbapi.com"
    static let scheme = "http"
    static let apiKey = "4bbfd5f1"
}

struct AuthService {

    private let session = URLSession.shared
//    private let endpoint = "authentication"

    func searchWithTitle(_ title: String, completion: (([MovieShort]) -> Void)?) {
//        guard let url = URL(string: Utils.mainURL + "?apiKey=\(Utils.apiKey)&s=\(title)") else { return }te
        var components = URLComponents(string: Utils.mainURL)
        components?.scheme = Utils.scheme
        components?.queryItems = [
            URLQueryItem(name: "apiKey", value: Utils.apiKey),
            URLQueryItem(name: "s", value: title)
        ]
        guard let url = components?.url else { return }
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        session.dataTask(with: request) { (data, response, error) in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            guard let response = response as? HTTPURLResponse, let data = data else { return }
            switch response.statusCode {
            case 200:
                do {
                    let movieResponse = try JSONDecoder().decode(MovieResponse.self, from: data)
                    completion?(movieResponse.search)
                } catch {
                    print(error.localizedDescription)
                }
            default:
                print("UNEXPECTED STATUS CODE")
            }
            print(data)
        }.resume()
    }
}
