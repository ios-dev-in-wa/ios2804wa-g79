//
//  TokenResponse.swift
//  ls16MovieApp
//
//  Created by WA on 23.06.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import Foundation

struct TokenResponse: Codable {
    let success: Bool
    let expiresAt, requestToken: String

    enum CodingKeys: String, CodingKey {
        case success
        case expiresAt = "expires_at"
        case requestToken = "request_token"
    }
}
