//
//  ViewController.swift
//  ls16MovieApp
//
//  Created by WA on 23.06.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(tableView)

        tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        tableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor).isActive = true

        let nib = UINib(nibName: "MovieShortTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "MovieShortTableViewCell")
        return tableView
    }()

    let searchController = UISearchController(searchResultsController: nil)

    private var movies = [MovieShort]()

    override func viewDidLoad() {
        super.viewDidLoad()
        // 1
        searchController.searchResultsUpdater = self
        // 2
        searchController.obscuresBackgroundDuringPresentation = false
        // 3
        searchController.searchBar.placeholder = "Search Movie"
        searchController.searchBar.searchTextField.delegate = self
        // 4
        navigationItem.searchController = searchController
        // 5
        definesPresentationContext = true
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return movies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "MovieShortTableViewCell", for: indexPath) as? MovieShortTableViewCell else { fatalError() }
        cell.setupWith(movie: movies[indexPath.row])
        return cell
    }
    
    
}

extension ViewController: UISearchResultsUpdating {
  func updateSearchResults(for searchController: UISearchController) {
    AuthService().searchWithTitle(searchController.searchBar.text ?? "") { [weak self] movies in
        self?.movies = movies
        DispatchQueue.main.async {
            self?.tableView.reloadData()
        }
    }
  }
}

extension ViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        AuthService().searchWithTitle(textField.text ?? "") { [weak self] movies in
            self?.movies = movies
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        }
        return true
    }
}
