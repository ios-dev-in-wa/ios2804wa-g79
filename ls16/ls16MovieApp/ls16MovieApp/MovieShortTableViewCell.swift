//
//  MovieShortTableViewCell.swift
//  ls16MovieApp
//
//  Created by WA on 23.06.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit
import Kingfisher

class MovieShortTableViewCell: UITableViewCell {

    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var movieTitleLabel: UILabel!
    @IBOutlet weak var movieYearLabel: UILabel!

    func setupWith(movie: MovieShort) {
        movieTitleLabel.text = movie.title
        movieYearLabel.text = movie.year
        posterImageView.kf.setImage(with: URL(string: movie.poster))
    }
}
