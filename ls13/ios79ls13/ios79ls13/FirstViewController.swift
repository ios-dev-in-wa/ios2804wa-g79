//
//  FirstViewController.swift
//  ios79ls13
//
//  Created by WA on 11.06.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit
import AVKit

class FirstViewController: UIViewController {
    
    var player: AVPlayer?

    override func viewDidLoad() {
        super.viewDidLoad()
//        if let urlForVideo = Bundle.main.url(forResource: "Twin-bell-alarm-clock", withExtension: "mp3") {
        if let urlForVideo = Bundle.main.url(forResource: "videoExample", withExtension: "mov") {
            player = AVPlayer(url: urlForVideo)

        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        let asset = AVAsset(url: Bundle.main.url(forResource: "videoExample", withExtension: "mov")!)
//        asset.
        let item = AVPlayerItem(asset: asset)
//        item.seek
        let layer = AVPlayerLayer(player: player)

        let box = UIView(frame: CGRect(x: 0, y: 0, width: 200, height: 200))
        view.addSubview(box)
        box.center = view.center

        layer.backgroundColor = UIColor.white.cgColor
        layer.frame = box.bounds
        layer.videoGravity = .resizeAspectFill

        box.layer.addSublayer(layer)
//        layer.repeatCount = .greatestFiniteMagnitude

        player?.play()
    }


    @IBAction func repeatButtonAction(_ sender: UIButton) {
        if let player = player {
            let time = CMTime(seconds: 0, preferredTimescale: player.currentTime().timescale)
            player.seek(to: time)
            player.rate = 0.25
//            player.play()
        }
    }
}

