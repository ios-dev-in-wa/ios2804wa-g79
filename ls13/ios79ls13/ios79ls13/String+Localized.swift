//
//  String+Localized.swift
//  ios79ls13
//
//  Created by WA on 11.06.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import Foundation

extension String {
    /// Returns localized version of the 'String'
    var localized: String {
        return NSLocalizedString(self, comment: "key")
    }
}
