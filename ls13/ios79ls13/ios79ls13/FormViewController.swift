//
//  FormViewController.swift
//  ios79ls13
//
//  Created by WA on 11.06.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

class FormViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var nameTextfield: UITextField!
    @IBOutlet weak var surnameTextfield: UITextField!
    @IBOutlet weak var dateOfBirthTextField: UITextField!
    @IBOutlet weak var workplaceTextField: UITextField!
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var phoneNumberTextField: UITextField!
    @IBOutlet weak var oneTextField: UITextField!
    @IBOutlet weak var secondTextField: UITextField!
    @IBOutlet weak var lastTextField: UITextField!

    var textFields: [UITextField] {
        return [ nameTextfield, surnameTextfield, dateOfBirthTextField, workplaceTextField, cityTextField, phoneNumberTextField, oneTextField, secondTextField, lastTextField]
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardSizeChanged), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardSizeChanged), name: UIResponder.keyboardWillHideNotification, object: nil)
        textFields.forEach {
            $0.delegate = self
        }
        setupLocatiozation()
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

//    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        super.touchesBegan(touches, with: event)
//        view.endEditing(true)
//    }

    @objc func keyboardSizeChanged(notification: Notification) {
        print(notification, notification.userInfo)
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            scrollView.contentInset.bottom = keyboardHeight
            print(keyboardHeight)
        }
    }

    func setupLocatiozation() {
        nameTextfield.placeholder = "nameKey".localized
        surnameTextfield.placeholder = "surnameKey".localized
        dateOfBirthTextField.placeholder = "Date of birth".localized
    }
}

extension FormViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true //(textField.text?.count ?? -1) < 5
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        guard let index = textFields.firstIndex(of: textField), index + 1 < textFields.count else { return true }
        textField.resignFirstResponder()
        textFields[index + 1].becomeFirstResponder()
        return true
    }
}
