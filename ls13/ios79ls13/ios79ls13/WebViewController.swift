//
//  WebViewController.swift
//  ios79ls13
//
//  Created by WA on 11.06.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit
import WebKit
import AVKit

class WebViewController: UIViewController {

    @IBOutlet weak var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // LOAD RAW HTML CODE
        
//        webView.loadHTMLString(
//            """
//<!DOCTYPE html>
//<html>
//<body>
//
//<h1>My First Heading</h1>
//<p>My first paragraph.</p>
//
//</body>
//</html>
//""", baseURL: nil)

//        webView.load(URLRequest(url: URL(string: "https://youtube.com")!))
        if let urlForVideo = Bundle.main.url(forResource: "videoExample", withExtension: "mov") {
            webView.loadFileURL(urlForVideo, allowingReadAccessTo: urlForVideo)
        }
//        webView.
        webView.uiDelegate = self
        webView.navigationDelegate = self
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if segue.identifier == "goToAVPlayerVC", let destination = segue.destination as? AVPlayerViewController {
            if let urlForVideo = Bundle.main.url(forResource: "videoExample", withExtension: "mov") {
                let player = AVPlayer(url: urlForVideo)
                
                destination.player = player
            }
        }
    }
}

extension WebViewController: WKUIDelegate, WKNavigationDelegate {
    
}
