//
//  AddCarViewController.swift
//  ios79ls11
//
//  Created by WA on 04.06.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit
import Parse

class AddCarViewController: UIViewController {

    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var wasInAccidentSwitch: UISwitch!
    @IBOutlet weak var productionYearTextField: UITextField!

    var didSaveObject: (() -> Void)?
    
    @IBAction func didPressSave(_ sender: UIButton) {
        CarManager.shared.saveCar(title: titleTextField.text ?? "NO name",
                                  wasInAccident: wasInAccidentSwitch.isOn,
                                  productionYear: productionYearTextField.text) {
                                    self.didSaveObject?()
                                    self.dismiss(animated: true)
                                    print("CAR IS CREATED")
        }
    }
}
