//
//  CarManager.swift
//  ios79ls11
//
//  Created by WA on 04.06.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import Foundation
import Parse

struct Car {
    let title: String
    let wasInAccident: Bool
    let productionYear: String?
}

class CarManager {

    static let shared = CarManager()

    func saveCar(title: String, wasInAccident: Bool, productionYear: String?, completion: @escaping (() -> Void)) {
        let carObject = PFObject(className: "Car")
        
        carObject["title"] = title
        carObject["wasInAccident"] = wasInAccident
        carObject["productionYear"] = productionYear
    
        carObject.saveInBackground { (isSuccess, error) in
            if let error = error {
                print(error.localizedDescription)
            }
            if isSuccess {
                completion()
            }
        }
    }

    func getCars(completion: @escaping (([PFObject]) -> Void)) {
        let query = PFQuery(className: "Car")

        _ = query.findObjectsInBackground { (objects, error) in
            if let error = error {
                print(error.localizedDescription)
            }
            if let objects = objects {
                completion(objects)
            }
        }
    }
}
