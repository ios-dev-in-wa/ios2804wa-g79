//
//  DetailViewController.swift
//  ios79ls11
//
//  Created by WA on 04.06.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var detailDescriptionLabel: UILabel!

    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        if traitCollection.horizontalSizeClass == .compact {
            print("width SIZE CLASS IS COMPACT")
        }
    }

    func configureView() {
        // Update the user interface for the detail item.
        if let detail = detailItem {
            if let label = detailDescriptionLabel {
                label.text = detail.description
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        configureView()
        setupNavigationButtons()
    }
    
//    var newData: Date? {
//        didSet {
//
//        }
//    }

    var detailItem: NSDate? {
        didSet {
            // Update the view.
            print("New data is set")
            configureView()
        }
    }

    
    func setupNavigationButtons() {
        let barButton = UIBarButtonItem(barButtonSystemItem: .bookmarks, target: self, action: #selector(didPressBookmarks))
        navigationItem.rightBarButtonItem = barButton
    }

    @objc func didPressBookmarks() {
        performSegue(withIdentifier: "goToNextVC", sender: nil)
    }

}

