//
//  UIColor+Extentions.swift
//  g79ls5
//
//  Created by WA on 12.05.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

extension UIColor {
    static var random: UIColor {
        return UIColor(red: .random(in: 0...1),
                       green: .random(in: 0...1),
                       blue: .random(in: 0...1),
                       alpha: 1.0)
    }
}
