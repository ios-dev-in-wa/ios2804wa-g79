//
//  Robot.swift
//  g79ls5
//
//  Created by WA on 12.05.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import Foundation

class Robot {
    let name: String
    private(set) var health: Double = 100

    var tag: String {
        return "\(name)--\(health)"
    }

    init(name: String) {
        self.name = name
    }

    deinit {
        print("Robot with \(name) is deinited.")
    }

    func move() {
        print("Robot Move forward")
    }

    func getDamage(dmg: Double) {
        health -= dmg
        if health <= 0 {
            print("Robot is dead :(")
        }
    }
}

class CandyRobot: Robot {
    var candyBag: Int = 1

    func put() {
        if candyBag != 0 {
            print("Robot put a candy")
            candyBag -= 1
        } else {
            print("No candy in bag")
        }
    }
    
    func pick() {
        candyBag += 1
        print("Robot pick candy, current candies: \(candyBag)")
    }
}
