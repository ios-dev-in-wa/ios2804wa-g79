//
//  ViewController.swift
//  g79ls5
//
//  Created by WA on 12.05.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var firstRobotLabel: UILabel!
    @IBOutlet weak var secondRobotLabel: UILabel!
    @IBOutlet weak var deleteRobotButton: UIButton!
    var robot: CandyRobot? = CandyRobot(name: "C3-PO")
    var firstBattleRobot = BattleRobot(roar: "Wooah", name: "DROIDEKA")
    var secondBattleRobot = BattleRobot(roar: "Wooah", name: "Robo-cop")

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        for _ in 0...20 {
            robot?.put()
        }

//        let battleRobot = BattleRobot(roar: <#T##String#>, name: <#T##String#>)
//        battleRobot.attack()

        let battleRobotTwo = BattleRobot(roar: "SSS")
        let battleRobotTwoT = BattleRobot(roar: "AAA")
        battleRobotTwoT.getDamage(dmg: 1)
        print(battleRobotTwoT.tag)
        battleRobotTwoT.getDamage(dmg: 10)
        print(battleRobotTwoT.tag)
        print(battleRobotTwoT.health, battleRobotTwoT.name)
    }

    func setRobotStatus() {
        let generator = UIImpactFeedbackGenerator(style: .heavy)
        generator.impactOccurred()
        firstRobotLabel.text = firstBattleRobot.status
        secondRobotLabel.text = secondBattleRobot.status
    }

    @IBAction func didTapDeleteButton(_ sender: UIButton) {
        print("Button is tapped")
        robot = nil
        deleteRobotButton.backgroundColor = UIColor.random
        view.backgroundColor = .random
    }
    
    @IBAction func makeFightAction(_ sender: Any) {
        guard firstBattleRobot.health > 0, secondBattleRobot.health > 0 else {
            firstBattleRobot = BattleRobot(roar: "Heyeeee", name: "Cowboy")
            secondBattleRobot = BattleRobot(roar: "Heyeettee", name: "Jr. Cowboy")
            setRobotStatus()
            return
        }
        firstBattleRobot.getDamage(dmg: Double.random(in: 1...20))
        secondBattleRobot.getDamage(dmg: Double.random(in: 1...20))
        setRobotStatus()
    }
}

