//
//  BattleRobot.swift
//  g79ls5
//
//  Created by WA on 12.05.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import Foundation

class BattleRobot: Robot {

    var isInButtle: Bool = false
    var roar: String = ""

    var status: String {
        guard health > 0 else { return "\(name) is Dead." }
        return "\(name) current health: \(health)"
    }

    func attack() {
    }

    init() {
        roar = "Haha"
        super.init(name: "haha-bot")
    }

    init(roar: String, name: String) {
        self.roar = roar
        super.init(name: name)
    }

    init(roar: String) {
        self.roar = roar
        super.init(name: "ha-ha-ha-ha")
    }

    override func move() {
        print("BLa bla move")
    }

//    override func pick() {
//        super.pick()
//        print("\(name) couldn't pick")
//    }
}
