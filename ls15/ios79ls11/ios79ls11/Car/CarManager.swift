//
//  CarManager.swift
//  ios79ls11
//
//  Created by WA on 04.06.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import Foundation
import Parse

class CarManager {

    static let shared = CarManager()

    private var service: CarService {
        // no connection
        return CarLocalService(context: Utils.appDelegate.persistentContainer.viewContext)
        
        // want to use ParseObjects
        // return CarParseService()

//        return CarRESTService()
    }

    func saveCar(title: String, wasInAccident: Bool, productionYear: String?, completion: @escaping ((Car?) -> Void)) {
        service.createCar(car: Car(title: title, wasInAccident: wasInAccident, objectId: "", productionYear: productionYear), completion: completion)
//        let carObject = PFObject(className: "Car")
//
//        carObject["title"] = title
//        carObject["wasInAccident"] = wasInAccident
//        carObject["productionYear"] = productionYear
//
//        carObject.saveInBackground { (isSuccess, error) in
//            if let error = error {
//                print(error.localizedDescription)
//            }
//            if isSuccess {
//                completion()
//            }
//        }
    }

    func getCars(completion: @escaping (([Car]) -> Void)) {
        service.readCars(completion: completion)
//        let query = PFQuery(className: "Car")
//
//        _ = query.findObjectsInBackground { (objects, error) in
//            if let error = error {
//                print(error.localizedDescription)
//            }
//            if let objects = objects {
//                completion(objects.compactMap { Car(object: $0) })
//            }
//        }
    }

    func delete(car: Car) {
        let query = PFQuery(className:"Car")

        query.getObjectInBackground(withId: car.objectId) { (parseObject: PFObject?, error: Error?) -> Void in
            if let error = error {
                print(error.localizedDescription)
            } else if let parseObject = parseObject {
                parseObject.deleteInBackground()
            }
        }
    }

    func update(car: Car, completion: ((Car) -> Void)?) {
        let query = PFQuery(className:"Car")
        
        query.getObjectInBackground(withId: car.objectId) { (parseObject: PFObject?, error: Error?) -> Void in
            if let error = error {
                print(error.localizedDescription)
            } else {
                parseObject?["title"] = car.title
                parseObject?["wasInAccident"] = car.wasInAccident
                parseObject?["productionYear"] = car.productionYear
            }
            parseObject?.saveInBackground(block: { _, error in
                if let error = error {
                    print(error.localizedDescription)
                } else {
                    guard let object = parseObject, let car = Car(object: object) else { return }
                    completion?(car)
                }
            })
        }
    }
}
