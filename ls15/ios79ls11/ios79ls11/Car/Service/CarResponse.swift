//
//  CarResponse.swift
//  ios79ls11
//
//  Created by WA on 09.06.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import Foundation

struct CarResponse: Codable {
    let results: [Car]
}
