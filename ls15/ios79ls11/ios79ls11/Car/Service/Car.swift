//
//  Car.swift
//  ios79ls11
//
//  Created by WA on 09.06.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import Foundation
import Parse
import CoreData

struct Car: Codable {
    let title: String
    let wasInAccident: Bool
    let objectId: String
    let productionYear: String?

    init?(object: PFObject) {
        guard let title = object["title"] as? String,
            let wasInAccident = object["wasInAccident"] as? Bool,
            let objectId = object.objectId else { return nil }
        self.title = title
        self.wasInAccident = wasInAccident
        self.objectId = objectId
        self.productionYear = object["productionYear"] as? String
    }

    init(title: String,
         wasInAccident: Bool,
         objectId: String,
         productionYear: String?) {
        self.title = title
        self.wasInAccident = wasInAccident
        self.objectId = objectId
        self.productionYear = productionYear
    }

    init(object: CarEntity) {
        self.title = object.title ?? "No name"
        self.wasInAccident = object.wasInAccident
        self.objectId = object.identifier ?? UUID().uuidString
        self.productionYear = object.productionYear
    }
}
