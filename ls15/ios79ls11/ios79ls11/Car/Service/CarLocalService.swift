//
//  CarLocalService.swift
//  ios79ls11
//
//  Created by WA on 18.06.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import CoreData

struct CarLocalService: CarService {

    private let context: NSManagedObjectContext

    init(context: NSManagedObjectContext) {
        self.context = context
    }

    func createCar(car: Car, completion: ((Car?) -> Void)?) {
        let carEntity = NSEntityDescription.insertNewObject(forEntityName: "CarEntity", into: context) as! CarEntity
        carEntity.identifier = car.objectId.isEmpty ? UUID().uuidString : car.objectId
        carEntity.productionYear = car.productionYear
        carEntity.wasInAccident = car.wasInAccident
        carEntity.title = car.title

        do {
            try context.save()
            completion?(car)
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func readCars(completion: (([Car]) -> Void)?) {
        let fetchRequest = NSFetchRequest<CarEntity>(entityName: "CarEntity")
        fetchRequest.fetchBatchSize = 20
        do {
            let results = try context.fetch(fetchRequest)
            let cars = results.compactMap { Car(object: $0)}
            completion?(cars)
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func updateCar(car: Car, completion: ((Car) -> Void)?) {
        let fetchRequest = NSFetchRequest<CarEntity>(entityName: "CarEntity")

        fetchRequest.predicate = NSPredicate(format: "identifier = '\(car.objectId)'")
        do {
            let results = try context.fetch(fetchRequest)
            if let oneCar = results.first {
                oneCar.title = car.title
                oneCar.productionYear = car.productionYear
                oneCar.wasInAccident = car.wasInAccident
                
                completion?(Car(object: oneCar))
            }
            try context.save()
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func deleteCar(car: Car, completion: ((Car) -> Void)?) {
        let fetchRequest = NSFetchRequest<CarEntity>(entityName: "CarEntity")

        fetchRequest.predicate = NSPredicate(format: "identifier = '\(car.objectId)'")
        do {
            let results = try context.fetch(fetchRequest)
            if let oneCar = results.first {
                context.delete(oneCar)
                completion?(Car(object: oneCar))
            }
            try context.save()
        } catch {
            print(error.localizedDescription)
        }
    }

}
