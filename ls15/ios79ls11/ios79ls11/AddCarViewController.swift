//
//  AddCarViewController.swift
//  ios79ls11
//
//  Created by WA on 04.06.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit
import Parse

class AddCarViewController: UIViewController {

    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var wasInAccidentSwitch: UISwitch!
    @IBOutlet weak var productionYearTextField: UITextField!

    var isInEditMode = false
    var currentCar: Car?
    var didSaveObject: (() -> Void)?

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        titleTextField.text = currentCar?.title
        wasInAccidentSwitch.isOn = currentCar?.wasInAccident ?? true
        productionYearTextField.text = currentCar?.productionYear
    }
    
    @IBAction func didPressSave(_ sender: UIButton) {
        if isInEditMode {
            guard let lastCar = currentCar else { return }
            CarManager.shared.update(car: Car(title: titleTextField.text ?? lastCar.title,
                                              wasInAccident: wasInAccidentSwitch.isOn,
                                              objectId: lastCar.objectId,
                                              productionYear: productionYearTextField.text ?? lastCar.productionYear)) { [weak self ] _ in
                                                self?.didSaveObject?()
                                                self?.dismiss(animated: true)
                                                print("CAR IS UPDATED")
            }
        } else {
            CarManager.shared.saveCar(title: titleTextField.text ?? "NO name",
                                      wasInAccident: wasInAccidentSwitch.isOn,
                                      productionYear: productionYearTextField.text) { _ in
                                        self.didSaveObject?()
                                        self.dismiss(animated: true)
                                        print("CAR IS CREATED")
            }
        }
    }
}
