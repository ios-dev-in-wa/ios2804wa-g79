//
//  ViewController.swift
//  g79ls6
//
//  Created by WA on 19.05.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var textField: UITextField!
    let urlForFile = "/Users/wa/Desktop/saved.plist"
    let button = UIButton()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        saveExample(array: ["Aaa", "BBb"])
        var readedArray = readExample()
        readedArray.insert("FirstElement", at: 0)
        saveExample(array: readedArray)

//        let settings = Settings.shared

//        UIScreen.main.bounds
//        UIDevice.current.
//        print(NSHomeDirectory())

//        let color = UIColor(red: 0.03, green: 0.40, blue: 0.22, alpha: 1.0)
        view.backgroundColor = .newColor

//        imageView.image = UIImage(named: "notsadcat")
        let imageCat = UIImage(named: "notsadcat")
        if let data = imageCat?.jpegData(compressionQuality: 1.0) {
            try? data.write(to: URL(fileURLWithPath: "/Users/wa/Desktop/image.jpeg"))
        }

        if let data = try? Data(contentsOf: URL(fileURLWithPath: "/Users/wa/Desktop/image.jpeg")) {
            let image = UIImage(data: data)
            imageView.image = image
        }

        var documetsUrl = FileManager.default.urls(for: .documentDirectory, in: .allDomainsMask).first
        documetsUrl?.appendPathComponent("image.jpeg")
        print(documetsUrl)
//        textView.text = "OMGGG gggg"
//        textView.font = .mainFont18
        let attributedString = NSMutableAttributedString(string: "Hello, dear friend")
        let range = NSRange(location: 0, length: 5)
        attributedString.addAttributes([
            .font: UIFont.mainFont18,
            .foregroundColor: UIColor.red,
            .underlineStyle: NSUnderlineStyle.single.rawValue
        ], range: range)
        textView.attributedText = attributedString

//        let buttonView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
//        buttonView.addSubview(button)
//        button.frame = buttonView.bounds
        button.addTarget(self, action: #selector(didPressEyeButton), for: .touchUpInside)
        button.setImage(UIImage(systemName: "eye"), for: .normal)
        button.tintColor = textField.textColor
        textField.rightView = button
        textField.rightViewMode = .always

    }

    @objc func didPressEyeButton() {
        textField.isSecureTextEntry.toggle()
        button.setImage(UIImage(systemName: textField.isSecureTextEntry ? "eye" : "eye.slash"), for: .normal)
    }

    func saveExample(array: [String]) {
        let arrayOfNames = array as NSArray
        let fileUrl = URL(fileURLWithPath: urlForFile)
        do {
            try arrayOfNames.write(to: fileUrl)
        } catch {
            print(error.localizedDescription)
        }
    }

    func readExample() -> [String] {
        let fileUrl = URL(fileURLWithPath: urlForFile)
        if let readedArray = NSArray(contentsOf: fileUrl) as? [String] {
            print(readedArray)
            return readedArray
        }
        return []
    }

}

