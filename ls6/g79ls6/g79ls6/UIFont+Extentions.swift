//
//  UIFont+Extentions.swift
//  g79ls6
//
//  Created by WA on 19.05.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

extension UIFont {

    static let mainFont18: UIFont = UIFont(name: "BalsamiqSans-Bold", size: 32)!
}
