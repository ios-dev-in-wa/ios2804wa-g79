//
//  Settings.swift
//  g79ls6
//
//  Created by WA on 19.05.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import Foundation

class Settings {

    private init() {}

    static var shared = Settings()

    private let defaults = UserDefaults.standard

    var isFirstLaunch: Bool {
        get {
            return (defaults.value(forKey: "isFirstLaunch") as? Bool) ?? true
        }
        set {
            defaults.set(newValue, forKey: "isFirstLaunch")
        }
    }

    var launchCounter: Int {
        get {
            return (defaults.value(forKey: "launchCounter") as? Int) ?? 0
        }
        set {
            defaults.set(newValue, forKey: "launchCounter")
        }
    }
}
