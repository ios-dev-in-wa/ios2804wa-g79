//
//  UIColor+Extentions.swift
//  g79ls6
//
//  Created by WA on 19.05.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

extension UIColor {
    static var random: UIColor {
        return UIColor(red: .random(in: 0...1),
                       green: .random(in: 0...1),
                       blue: .random(in: 0...1),
                       alpha: 1.0)
    }

    static var olive: UIColor = UIColor(red: 0.03, green: 0.40, blue: 0.22, alpha: 1.0)

    static var newColor: UIColor = UIColor(red: 0.83, green: 0.40, blue: 0.22, alpha: 1.0)
}
