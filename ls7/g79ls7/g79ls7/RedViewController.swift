//
//  RedViewController.swift
//  g79ls7
//
//  Created by WA on 21.05.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

class RedViewController: UIViewController {
    
    @IBOutlet weak var textLabel: UILabel!

    var arrayOfNames = [String]()

    override func loadView() {
        super.loadView()
        print("View loading")
//        let button = UINavigationController()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        print("View Did Load")
        // Do any additional setup after loading the view.
//        view.backgroundColor = .green
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("View Will Appear ")
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print("View Did Appear ")
//        view.backgroundColor = .red
        print(arrayOfNames)
    }

//    override func didReceiveMemoryWarning() {
//
//    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        print("View Will Disappear")
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        print("View Did Disappear")
    }

    deinit {
        print("Deinit")
    }

    @IBAction func reduButtonAction(_ sender: UIButton) {
        if let newName = textLabel.text, !newName.isEmpty {
            arrayOfNames.append(newName)
        }
        performSegue(withIdentifier: "goToBoomVC", sender: arrayOfNames)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if segue.identifier == "goToBoomVC", let destinationVC = segue.destination as? BoomViewController, let arrayOfNamesFromSender = sender as? [String] {
            destinationVC.arrayOfNames = arrayOfNamesFromSender
            destinationVC.delegate = self
        }
    }
}

extension RedViewController: BoomViewControllerDelegate {
    func boomViewController(chooseName: String) {
        textLabel.text = "Choosen name is: \(chooseName)"
        print(chooseName)
    }
}
