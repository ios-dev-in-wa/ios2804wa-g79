//
//  BoomViewController.swift
//  g79ls7
//
//  Created by WA on 21.05.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

protocol BoomViewControllerDelegate: class {
    func boomViewController(chooseName: String)
}

class BoomViewController: UIViewController {

    @IBOutlet weak var pressMeButton: UIButton!
    @IBOutlet weak var textField: UITextField!
    var arrayOfNames = [String]()
    var delegate: BoomViewControllerDelegate?

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print(arrayOfNames)
    }

    @IBAction func pressMeAction(_ sender: UIButton) {
        let index = Int.random(in: 0...arrayOfNames.count - 1)
        let choosenName = arrayOfNames[index]
        delegate?.boomViewController(chooseName: choosenName)
        navigationController?.popViewController(animated: true)
//        let newVC = UIViewController()
//        newVC.title = "PinkGuy"
//        newVC.view.backgroundColor = .systemPink
        
//        navigationController?.pushViewController(newVC, animated: true)
//        print("Button is pressed")
    }

    @IBAction func saveAction(_ sender: UIButton) {
        NotificationCenter.default.post(name: .boomViewControllerNotification, object: nil)
    }
}

extension Notification.Name {
    static let boomViewControllerNotification: NSNotification.Name = NSNotification.Name(rawValue: "BoomViewControllerNotification")
}
