//
//  ViewController.swift
//  g79ls7
//
//  Created by WA on 21.05.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var textField: UITextField!

    let arrayOfNames = ["Ihor", "Alla", "Smarty"]
    
    override func loadView() {
        super.loadView()
        print("White View loading")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("White View Did Load")
        // Do any additional setup after loading the view.
        view.backgroundColor = .green
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("White View Will Appear ")
        NotificationCenter.default.addObserver(self, selector: #selector(orientationDidChange), name: UIDevice.orientationDidChangeNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillAppear), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(boomViewAction), name: .boomViewControllerNotification, object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print("White View Did Appear ")
//        view.backgroundColor = .red
    }
    
    //    override func didReceiveMemoryWarning() {
    //
    //    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        print("White View Will Disappear")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        print("White View Did Disappear")
        NotificationCenter.default.removeObserver(self)
    }
    
    deinit {
        print("White Deinit")
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if segue.identifier == "segueToRedVC", let destinationVC = segue.destination as? RedViewController {
            // Force view load
            _ = destinationVC.view
            //
            destinationVC.textLabel.text = textField.text
            destinationVC.arrayOfNames = arrayOfNames
        }
    }

    @objc func orientationDidChange() {
        print("Orientation did Change")
    }

    @objc func keyboardWillAppear() {
        print("Keyboard will Appear")
    }

    @objc func boomViewAction() {
        print("boomViewAction notification received")
        textField.text = "Boom button pressed"
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        view.endEditing(true)
    }
}

