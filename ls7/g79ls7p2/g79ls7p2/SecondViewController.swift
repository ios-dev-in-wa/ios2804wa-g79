//
//  SecondViewController.swift
//  g79ls7p2
//
//  Created by WA on 21.05.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if segue.identifier == "GoToModalVC", let modalVC = segue.destination as? ModalViewController {
            modalVC.delegate = self
        }
    }
}

extension SecondViewController: ModalViewControllerDelegate {
    func modalViewControllerDismissed() {
//        if tabBarController?.selectedViewController == self {
        tabBarController?.selectedIndex = 0
//        }
    }
}
