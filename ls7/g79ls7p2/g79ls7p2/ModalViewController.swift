//
//  ModalViewController.swift
//  g79ls7p2
//
//  Created by WA on 21.05.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

protocol ModalViewControllerDelegate: class {
    func modalViewControllerDismissed()
}

class ModalViewController: UIViewController {

    weak var delegate: ModalViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func randomButtonAction(_ sender: UIButton) {
        if navigationController == nil {
            print("ModalViewController doesn't have navigation controller")
            
            // Will not work
//            navigationController?.popViewController(animated: true)
            
            // Will work
            dismiss(animated: true, completion: {
                self.delegate?.modalViewControllerDismissed()
                print("ModalViewController dismissed")
            })
        }
    }
}
