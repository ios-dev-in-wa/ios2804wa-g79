//
//  Car.swift
//  g79ls4
//
//  Created by WA on 07.05.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

class Car {

    enum BodyType: Int {
        case coope = 100
        case sedan = 200
        case hatchback = 300

        var name: String {
            switch self {
            case .coope: return "Купе"
            case .sedan: return "Седан"
            case .hatchback: return "Хэчбэк"
            }
        }

        var doorCount: Int {
            switch self {
            case .coope: return 2
            case .hatchback: return 5
            case .sedan: return 4
            }
        }
    }

    var color: UIColor
    let bodyType: BodyType

    init(color: UIColor, bodyType: BodyType) {
        self.color = color
        self.bodyType = bodyType
    }

    func move() {
        print("Car moves front")
    }

    func stop() {
        print("Car stops")
    }

    func startEngine() {
        
    }
}
