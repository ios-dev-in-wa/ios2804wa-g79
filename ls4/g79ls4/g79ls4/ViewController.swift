//
//  ViewController.swift
//  g79ls4
//
//  Created by WA on 07.05.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
//        stringExample()
//        arrayExample()
//        dictionaryExample()
        view.backgroundColor = .red

        let car = Car(color: .green, bodyType: .sedan)
        view.backgroundColor = car.color

        let redCar = Car(color: .red, bodyType: .coope)
        print(car.bodyType.name, car.bodyType.doorCount, redCar.bodyType.name)

        let sedanBodyType = Car.BodyType(rawValue: 200)
        print(sedanBodyType?.name)
        car.color = .black
        print(car.bodyType.rawValue)

        let profile = Profile(name: "Artem", phoneNumber: "23333", gender: .male)
        print(profile.gender.rawValue)
    }
    
    func stringExample() {
        let name = "Artem"
        let surname = """
    asdfmkasdkmflkm
    alfmskdlmfasdf
    famksdfmlksd
"""
        print(name.isEmpty, "".isEmpty)

        print(name.count)
        
        print(name.prefix(3), name.hasPrefix("Has"))

        print(surname.replacingOccurrences(of: "asdf", with: "Hello"))

        print(name.firstIndex(of: "A"))

        let nameLong = "Artem Hey, kuku A"
        print(nameLong.uppercased())
    }

    func arrayExample() {
        let firstName = "Artem"
        let secondName = "Inna"
        let thirdName = "Ihor"

        var arrayOfNames = [firstName, secondName, thirdName]

        let numberOne = 1
        let numberTwo = 2
        let numberThree = 3

        let arrayOfNumbers = [numberOne, numberTwo, numberThree]

//        let arrayOfAny: [Any] = ["asfasdf", 1, 2.0]

        let arrayOfDoubles = [1.2, 3.4, 5.5]

        print(arrayOfNames)
        arrayOfNames.append("Alona")
        print(arrayOfNames)

        let indexToGet = 2
        if indexToGet < arrayOfNames.count {
            let firstElement = arrayOfNames[indexToGet]
            print(firstElement)
        } else {
            print(indexToGet, "is out of range")
        }

        // arrayOfNames.insert("Alfa", at: 55) '55 is out of range'
        arrayOfNames.insert("Alfa", at: 1)
        print(arrayOfNames)

        arrayOfNames.remove(at: 1)
        print(arrayOfNames)

        let index = arrayOfNames.firstIndex(of: "Ihor")
        print(index)

        for element in arrayOfNames {
            print(element)
        }

        print("-------------------------")
        arrayOfNames.forEach { string in
            print(string)
        }

        let filtered = arrayOfNames.filter { name -> Bool in
            return name.contains("Ishor")
        }
        print(filtered)

        let sorted = arrayOfNames.sorted { first, second -> Bool in
            return first.count > second.count
        }
        print(sorted)

        print(arrayOfNames.sorted(by: <))

        print(arrayOfNumbers.sorted(by: >))

        let arrayOfDoubleMap = arrayOfNumbers.map { number -> String in
            return String(number)
        }
        print(arrayOfDoubleMap)
        print("Array countains element 1", arrayOfDoubleMap.contains("1"))
    }

    func dictionaryExample() {
        var contactBook = [
            "Police" : 101,
            "Ambulance" : 102,
            "Fire fighters": 103
        ]
        print(contactBook)
        if let number = contactBook["Police"] {
            print("Let's call Police, number - \(number)")
        }

        contactBook["Home"] = 123
        contactBook["Police"] = 911
        print(contactBook)

        var dict = [
            1 : ["Artem", "Inna"],
            2 : ["Alona"]
        ]
        print(dict[1])

        print(dict.count)
        print(dict.keys, dict.values)

//        for element in dict {
//            element.key
//        }

        let dictXY = [
            "X": 1,
            "Y": 1222
        ]

        let tupleExample = (x: 1, y: 1222)
        print(tupleExample.x)

        print(dictXY["X"], tupleExample.x)

        /// Remove all items from dict
        contactBook.removeAll()
        contactBook = [:]
    }
}

