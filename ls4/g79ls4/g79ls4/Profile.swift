//
//  Profile.swift
//  g79ls4
//
//  Created by WA on 07.05.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import Foundation

class Profile {

    enum Gender: String {
        case male = "Male"
        case female = "Female"
        case none = "None"
    }

    let name: String
    let phoneNumber: String
    let gender: Gender

    init(name: String, phoneNumber: String, gender: Gender) {
        self.name = name
        self.phoneNumber = phoneNumber
        self.gender = gender
    }
}

class Phone {
    enum Memory {
        case eight, sixteen, thirtyTwo
    }
}
